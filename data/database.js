var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');
var schema = mongoose.Schema;

var Event = new schema({
attending_count: { type: Number, default: '' },
   cover: {
      offset_x: { type: Number,default: 0 },
      offset_y: { type: Number,default: 0 },
      source: { type: String,default: '' },
      id: { type: String,default: '' }
   },
   description: { type: String, default: '' },
   interested_count: { type: Number,default: 0 },
   declined_count: { type: Number,default: 0 },
   name: { type: String,default: '' },
   owner: {
      name: { type: String,default: 'Cinema le Colisé' },
      id: { type: String,default: '' }
   },
   maybe_count: { type: Number,default: 0 },
   noreply_count: { type: Number,default: 0 },
   start_time: { type: Date,default: Date.now },
   place: {
      name: { type: String,default: 'Cinema le Colisé - Tunis -Rue de Marseille' }
     
   },
   type: { type: String,default: '' },
   guest_list_enabled: { type: Boolean,default: true},
   id: { type: String,default: '' },
   timezone: { type:String,default: '' },
   updated_time: { type: Date,default: Date.now },
   price: { type: Number, default: 0 }
});

var User = new schema({
first_name: String,
last_name: String
});

var ses = new schema({
username: String,
connected:Boolean,
iduser:String,
location: { latitude:String,longitude:String},
first: { type: String,default: '' },
last: { type: String,default: '' },
email: { type: String,default: '' },
phone: { type: Number, default: 0 }
});

var Account = new schema({
username: String,
password: String,
connected: Boolean,
location: { latitude:String ,
            longitude:String},
first:String,
last:String,
email:String,
phone:Number
});

var Comment = new schema({
username: String,
contenu: String,
date : {type:Date, default:Date.now}
});

var actualite = new schema ({
titre:String,
sujet:String,
date:Date,
image:String
});

mongoose.model('comment', Comment);
mongoose.model('users', User);
mongoose.model('events', Event);
mongoose.model('ses', ses);
mongoose.model('actualite',actualite);





Account.plugin(passportLocalMongoose);
module.exports = mongoose.model('Account', Account);


mongoose.connect('mongodb://localhost/pimean');
