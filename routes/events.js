var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Event = mongoose.model('events');

var eventFB = require('../FBdata/a');


router.get('/FB', function(req, res) {

var i;
var key, count = 0;
for(key in eventFB.es.data) {  
    count++;  
}

console.log(count);

for (i = 0; i < count; i++) {

new Event({
attending_count: eventFB.es.data[i].attending_count,
   cover: {
      offset_x: eventFB.es.data[i].cover.offset_x,
      offset_y: eventFB.es.data[i].cover.offset_y,
      source: (eventFB.es.data[i].cover.source).toString(),
      id: (eventFB.es.data[i].cover.id).toString()
   },
   description: (eventFB.es.data[i].description).toString(),
   interested_count: eventFB.es.data[i].interested_count,
   declined_count: eventFB.es.data[i].declined_count,
   name: (eventFB.es.data[i].name).toString(),
  
   maybe_count: eventFB.es.data[i].maybe_count,
   noreply_count: eventFB.es.data[i].noreply_count,
   start_time: eventFB.es.data[i].start_time,
  
   type: (eventFB.es.data[i].type).toString(),
   guest_list_enabled: eventFB.es.data[i].guest_list_enabled,
   id: (eventFB.es.data[i].id).toString(),
   timezone: eventFB.es.data[i].timezone,
   updated_time: eventFB.es.data[i].updated_time
}).save();
		  
 }		
	
	Event.find({},{},function(e,docs){
        res.json(docs);
    });
	 	 
});

router.get('/eventList', function(req, res) {

   
    Event.find({},{},function(e,docs){
        res.json(docs);
    });
	
});

router.post('/addEvent', function(req, res) {

    new Event({
   attending_count: req.body.attending_count,
   cover: {
      offset_x: req.body.cover.offset_x,
      offset_y: req.body.cover.offset_y,
      source: req.body.cover.source,
      id: req.body.cover.id
   },
   description: req.body.description,
   interested_count: req.body.interested_count,
   declined_count: req.body.declined_count,
   name: req.body.name,

   maybe_count: req.body.maybe_count,
   noreply_count: req.body.noreply_count,
   start_time: req.body.start_time,

   type: req.body.type,
   guest_list_enabled: req.body.guest_list_enabled,
   id: req.body.id,
   timezone: req.body.timezone,
   updated_time: req.body.updated_time,
   price: req.body.price
}).save(function(err, result){
        res.send("success to add event" );
    });
});

router.delete('/deleteEvent/:id', function(req, res) {
   
    var eventToDelete = req.params.id;
    Event.remove({ '_id' : eventToDelete }, function(err) {
        res.send("success to delete event");
    });
});

router.get('/searchEvent/:id', function(req, res) {
   
    var eventToSearch = req.params.id;
    Event.findById(eventToSearch , function(err,docs) {
	    if (err) {
		res.send("Failed to get event");
    } else {
      res.json(docs);
    }
	    
    });
});

router.get('/searchEventByName/:name', function(req, res) {
   
    var eventToSearch = req.params.name;
    Event.findOne({ 'name' : eventToSearch }, function(err,docs) {
	    if (err) {
		res.send("Failed to get event");
    } else {
      res.json(docs);
    }
	    
    });
});

router.put('/eventUpdate/:id', function(req, res) {
   
    var eventToUpdate = req.params.id;
	
	var updateDoc = req.body;
    delete updateDoc._id;
	
    Event.update({ '_id' : eventToUpdate }, updateDoc, function(err,docs) {
	    if (err) {
		res.send("Failed to update event");
    } else {
      res.send("success to update event");
    }
	    
    });
});





module.exports = router;