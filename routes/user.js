var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Account = require('../data/database');




router.get('/userlist', function(req, res) {

   
    Account.find({},{},function(e,docs){
        res.json(docs);
    });
	
});


router.delete('/deleteUser/:id', function(req, res) {
   
    var userToDelete = req.params.id;
    Account.remove({ '_id' : userToDelete }, function(err) {
        res.send("success to delete user");
    });
});

router.get('/searchUser/:id', function(req, res) {
   
    var UserToSearch = req.params.id;
    Account.findById(UserToSearch , function(err,docs) {
	    if (err) {
		res.send("Failed to get User");
    } else {
      res.json(docs);
    }
	    
    });
});

router.get('/searchUserByName/:name', function(req, res) {
   
    var UserToSearch = req.params.name;
    Account.findOne({ 'username' : UserToSearch }, function(err,docs) {
	    if (err) {
		res.send("Failed to get User");
    } else {
      res.json(docs);
    }
	    
    });
});

router.put('/UserUpdate/:id', function(req, res) {
   
    var UserToUpdate = req.params.id;
	
	var updateDoc = req.body;
    delete updateDoc._id;
	
    Account.update({ '_id' : UserToUpdate }, updateDoc, function(err,docs) {
	    if (err) {
		res.send("Failed to update user");
    } else {
      res.send("success to update user");
    }
	    
    });
});





module.exports = router;