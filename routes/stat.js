var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Event = mongoose.model('events');
var Comment = mongoose.model('comment');
var Account = require('../data/database');

router.get('/eventsCount', function(req, res) {
var key, count = 0;
Event.find({},{},function(e,docs){
      for(key in docs) {  
        count++;  
      }
      res.json({ value: count });
    });
	 	 
});


router.get('/commentsCount', function(req, res) {
var key, count = 0;
Comment.find({},{},function(e,docs){
      for(key in docs) {  
        count++;  
      }
      res.json({ value: count });
    });	 	 
});


router.get('/accountsCount', function(req, res) {
var key, count = 0;
Account.find({},{},function(e,docs){
      for(key in docs) {  
        count++;  
      }
      res.json({ value: count });
    });
	 	 
});

module.exports = router;