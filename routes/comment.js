var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Comment = mongoose.model('comment');



router.get('/commentlist', function(req, res) {

   
    Comment.find({},{},function(e,docs){
        res.json(docs);
    });
	
});



router.post('/addComment', function(req, res) {


    new Comment({
        
        username: req.body.username,
        contenu:  req.body.contenu,
        date: req.body.date
   
  
 
}).save(function(err, result){
        res.send("success to add comment" );
    });
});






router.delete('/deleteComment/:id', function(req, res) {
   
    var commentToDelete = req.params.id;
    Comment.remove({ '_id' : commentToDelete }, function(err) {
        res.send("success to delete comment");
    });
});

router.get('/searchComment/:id', function(req, res) {
   
    var CommentToSearch = req.params.id;
    Comment.findById(CommentToSearch , function(err,docs) {
	    if (err) {
		res.send("Failed to get Comments");
    } else {
      res.json(docs);
    }
	    
    });
});

router.get('/searchCommentByName/:name', function(req, res) {
   
    var CommentToSearch = req.params.name;
    Comment.findOne({ 'contenu' : CommentToSearch }, function(err,docs) {
	    if (err) {
		res.send("Failed to get Comment");
    } else {
      res.json(docs);
    }
	    
    });
});

router.put('/CommentUpdate/:id', function(req, res) {
   
    var CommentToUpdate = req.params.id;
	
	var updateDoc = req.body;
    delete updateDoc._id;
	
    Comment.update({ '_id' : CommentToUpdate }, updateDoc, function(err,docs) {
	    if (err) {
		res.send("Failed to update comment");
    } else {
      res.send("success to update comment");
    }
	    
    });
});






module.exports = router;