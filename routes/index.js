var express = require('express');
var mongoose = require('mongoose');
var ses = mongoose.model('ses');
var passport = require('passport');
var Account = require('../data/database');
var router = express.Router();

var jsdom = require('jsdom');
var fs = require("fs");
var jquery = fs.readFileSync("public/javascripts/global.js", "utf-8");

router.get('/loc2', function (req, res) {

var virtualConsole = jsdom.createVirtualConsole();
virtualConsole.on("log", function (message) {
  console.log("console.log called ->", message);
});

jsdom.env({  
  html:"",
  scripts: ['public/javascripts/jquery.min.js',
   'public/javascripts/global.js'
  ],  virtualConsole: virtualConsole ,  
done:function (err, window) {
  window.locateMe();
 console.log(window.lat,window.longi); // outputs Hello World
}});
});


router.get('/loc', function (req, res) {
//res.send( jquery.toString());
//console.log(jquery.hello); 
res.render('location.twig');
});

router.get('/getLocation', function (req, res) {

console.log(req.query.lat);
 ses.findById('570d2e15fd28c5a00aa8405c' , function(err,docs) {
	    if (err) {
		res.send("Failed to get session");
    } else {
	
	 Account.update({ '_id' :  docs.iduser }, {location: { latitude:req.query.lat,longitude:req.query.lng}}, function(err,docs) {
	    if (err) {
		res.send("Failed to geoloc");
    } else {  
	 res.send("success geloc");
	
    }
	    
    });
	
	
    }
	    
    });

   
});

router.put('/Location/:lat/:lng', function (req, res) {


 ses.findById('570d2e15fd28c5a00aa8405c' , function(err,docs) {
	    if (err) {
		res.send("Failed to get session");
    } else {
	
	 Account.update({ '_id' :  docs.iduser }, {location: { latitude:req.params.lat,longitude:req.params.lng}}, function(err,docs) {
	    if (err) {
		res.send("Failed to geoloc");
    } else {  
	 res.send("success geloc");
	
    }
	    
    });
	
	
    }
	    
    });

   
});

router.post('/register', function(req, res) {

Account.register(new Account({ username : req.body.username ,connected:true,location: { latitude:'',longitude:''}, first : req.body.first, last : req.body.last ,email : req.body.email, phone : req.body.phone}), req.body.password, function(err, account) {
if (err) {
res.send("err to register, user name existe deja" );
}
passport.authenticate('local')(req, res, function () {
res.send("success to register" );
});
});


});


router.post('/login', passport.authenticate('local'), function(req, res) {
    
	Account.update({ '_id' :  req.user.id }, {connected:true}, function(err,docs) {
	    if (err) {
		res.send("Failed to login user");
    } else {  

	res.send("success to login user");
    }
	    
    });


});

router.get('/getCurrentUser/:name', function(req, res) {


      var userToSearch = req.params.name;
      Account.findOne({ 'username' : userToSearch }, function(err,docs) {
	    if (err) {
		res.send("Failed to get event");
    } else {
    var updateDoc = docs;
    var idu= updateDoc._id;
	delete updateDoc._id;
	delete updateDoc.salt;
	delete updateDoc.hash;
	
	ses.update({ '_id' :  '570d2e15fd28c5a00aa8405c'}, {connected:updateDoc.connected,username:updateDoc.username,iduser:idu,location: { latitude:updateDoc.location.latitude,longitude:updateDoc.location.longitude},first:updateDoc.first,last:updateDoc.last,email:updateDoc.email,phone:updateDoc.phone}, function(err,docs) {
	    if (err) {
		res.send("Failed session");
    } 
	else {
    res.send("success session");
    }
	   });
    }
	    
    });
	


});

router.get('/getCurrentSession', function(req, res) {
    ses.findById('570d2e15fd28c5a00aa8405c' , function(err,docs) {
	    if (err) {
		res.send("Failed to get session");
    } else {
      res.json(docs);
    }
	    
    });

});


router.get('/auth/facebook',
  passport.authenticate('facebook'),
  function(req, res){});
router.get('/auth/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/' }),
  function(req, res) {
    res.redirect('/');
  });

router.get('/logout', function(req, res) {
req.logout();
res.redirect('/destroySession');
});


router.get('/destroySession', function(req, res) {

	ses.update({ '_id' :  '570d2e15fd28c5a00aa8405c'}, {username:'',connected:false,iduser:''}, function(err,docs) {
	    if (err) {
		res.send("Failed to logout user");
    } 
	else {
     res.send("Destroyeeed");
    }
	   });

});


module.exports = router;