var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
///////khaled
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var config = require('./models/oauth.js');
var FacebookStrategy = require('passport-facebook').Strategy;


var db = require('./data/database.js');
var routes = require('./routes/index');

var events = require('./routes/events');
var chat = require('./routes/chatRoom');
var user = require('./routes/user');
var users = require('./routes/users');
var comment = require('./routes/comment');
var stat = require('./routes/stat');
var act = require('./routes/actualite');


// serialize and deserialize
passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

// config
passport.use(new FacebookStrategy({
  clientID: config.facebook.clientID,
  clientSecret: config.facebook.clientSecret,
  callbackURL: config.facebook.callbackURL
  },
  function(accessToken, refreshToken, profile, done) {
    process.nextTick(function () {
      return done(null, profile);
    });
  }
));


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());

app.use(session({
secret: 'keyboard cat',
 rolling: true,
  saveUninitialized: true,
  resave: true,
 cookie: {
    maxAge: 20000000
  }

}));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

// Make our db accessible to our router
app.use(function(req,res,next){
    req.db = db;
	
  res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
}); 


app.use(function(req, res, next){
  if(req.session.login== undefined){
   
    req.session.login = req.user;
  }
  next();
});

app.use('/', routes);

app.use('/events', events);
app.use('/chat', chat);
app.use ('/user', user);
app.use ('/users',users);
app.use ('/comment',comment);
app.use ('/stat',stat);
app.use ('/actualite',act);

// passport config
var Account = require('./data/database');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
